package com.emirkuljanin.githubusers.ui

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.emirkuljanin.githubusers.util.NavigationUtils.performNavigateUp
import com.emirkuljanin.githubusers.util.UserDetailsUtils.checkUserDetailsBio
import com.emirkuljanin.githubusers.util.UserDetailsUtils.checkUserDetailsFollowers
import com.emirkuljanin.githubusers.util.UserDetailsUtils.checkUserDetailsFollowing
import com.emirkuljanin.githubusers.util.UserDetailsUtils.checkUserDetailsName
import com.emirkuljanin.githubusers.util.UserDetailsUtils.checkUserDetailsRepos
import com.emirkuljanin.githubusers.util.UserListUtils.checkRecyclerViewExists
import com.emirkuljanin.githubusers.util.UserListUtils.checkRecyclerViewItemsExist
import com.emirkuljanin.githubusers.util.UserListUtils.clickOnRecyclerViewItem
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class CheckUserDetailsTest {

    @Rule
    @JvmField
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun checkUserDetails() {
        //Wait for load
        Thread.sleep(4_000)

        checkRecyclerViewExists()
        checkRecyclerViewItemsExist()
        clickOnRecyclerViewItem()
        //Wait for load
        Thread.sleep(2_000)

        checkUserDetailsName()
        checkUserDetailsBio()
        checkUserDetailsFollowers()
        checkUserDetailsFollowing()
        checkUserDetailsRepos()
        performNavigateUp()
        checkRecyclerViewExists()
        checkRecyclerViewItemsExist()
    }
}