package com.emirkuljanin.githubusers.ui

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.emirkuljanin.githubusers.util.UserListUtils.checkRecyclerViewExists
import com.emirkuljanin.githubusers.util.UserListUtils.countRecyclerViewChildren
import com.emirkuljanin.githubusers.util.UserListUtils.scrollToItemAtPosition
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class LoadMoreUsersTest {
    private val lastItemIndex = 29
    private val itemCount = 30

    @Rule
    @JvmField
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun loadMoreUsersTest() {
        Thread.sleep(4_000)

        checkRecyclerViewExists()
        countRecyclerViewChildren(itemCount)
        scrollToItemAtPosition(lastItemIndex)

        Thread.sleep(2_000)
        checkRecyclerViewExists()
        countRecyclerViewChildren(itemCount * 2)
    }
}