package com.emirkuljanin.githubusers.navigation

import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.emirkuljanin.githubusers.ui.MainActivity
import com.emirkuljanin.githubusers.util.NavigationUtils.backPressed
import com.emirkuljanin.githubusers.util.NavigationUtils.performNavigateUp
import com.emirkuljanin.githubusers.util.UserListUtils.checkRecyclerViewExists
import com.emirkuljanin.githubusers.util.UserListUtils.clickOnRecyclerViewItem
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class NavigationTest {

    @Rule
    @JvmField
    val activityRule = ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun testNavigation() {
        //Wait for load
        Thread.sleep(4_000)

        checkRecyclerViewExists()
        clickOnRecyclerViewItem()

        //Wait for load
        Thread.sleep(1_000)
        performNavigateUp()

        checkRecyclerViewExists()
        clickOnRecyclerViewItem()

        //Wait for load
        Thread.sleep(1_000)
        backPressed()

        checkRecyclerViewExists()
    }
}