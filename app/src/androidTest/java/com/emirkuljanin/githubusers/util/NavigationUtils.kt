package com.emirkuljanin.githubusers.util

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import com.emirkuljanin.githubusers.R
import org.hamcrest.Matchers

object NavigationUtils {

    fun performNavigateUp() {
        val appCompatImageButton = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withContentDescription("Navigate up"),
                AutomationUtils.nthChildOf(
                    Matchers.allOf(
                        ViewMatchers.withId(R.id.action_bar),
                        AutomationUtils.nthChildOf(
                            ViewMatchers.withId(R.id.action_bar_container),
                            0
                        )
                    ),
                    2
                ),
                ViewMatchers.isDisplayed()
            )
        )
        appCompatImageButton.perform(ViewActions.click())
    }

    fun backPressed() {
        Espresso.pressBack()
    }
}