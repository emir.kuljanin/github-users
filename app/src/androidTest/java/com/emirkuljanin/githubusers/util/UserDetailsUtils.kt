package com.emirkuljanin.githubusers.util

import android.view.ViewGroup
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import com.emirkuljanin.githubusers.R
import org.hamcrest.Matchers
import org.hamcrest.core.IsInstanceOf

object UserDetailsUtils {

    fun checkUserDetailsName() {
        val textView = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.user_name_text_view),
                ViewMatchers.withParent(
                    Matchers.allOf(
                        ViewMatchers.withId(R.id.profile_header),
                        ViewMatchers.withParent(IsInstanceOf.instanceOf(ViewGroup::class.java))
                    )
                ),
                ViewMatchers.isDisplayed()
            )
        )
        textView.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    fun checkUserDetailsBio() {
        val textView = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.user_bio_text_view),
                ViewMatchers.withParent(
                    Matchers.allOf(
                        ViewMatchers.withId(R.id.profile_header),
                        ViewMatchers.withParent(IsInstanceOf.instanceOf(ViewGroup::class.java))
                    )
                ),
                ViewMatchers.isDisplayed()
            )
        )
        textView.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    fun checkUserDetailsFollowers() {
        val linearLayout =
            AutomationUtils.nthChildOf(ViewMatchers.withId(R.id.numbers_info_container), 0)

        val textView = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.number_text_view),
                ViewMatchers.withParent(linearLayout),
                ViewMatchers.isDisplayed()
            )
        )
        textView.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        val textView2 = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.label_text_view), ViewMatchers.withText("Followers"),
                ViewMatchers.withParent(linearLayout),
                ViewMatchers.isDisplayed()
            )
        )
        textView2.check(ViewAssertions.matches(ViewMatchers.withText("Followers")))
    }

    fun checkUserDetailsFollowing() {
        val linearLayout =
            AutomationUtils.nthChildOf(ViewMatchers.withId(R.id.numbers_info_container), 1)

        val textView = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.number_text_view),
                ViewMatchers.withParent(linearLayout),
                ViewMatchers.isDisplayed()
            )
        )
        textView.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        val textView2 = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.label_text_view), ViewMatchers.withText("Following"),
                ViewMatchers.withParent(linearLayout),
                ViewMatchers.isDisplayed()
            )
        )
        textView2.check(ViewAssertions.matches(ViewMatchers.withText("Following")))
    }

    fun checkUserDetailsRepos() {
        val linearLayout =
            AutomationUtils.nthChildOf(ViewMatchers.withId(R.id.numbers_info_container), 2)

        val textView = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.number_text_view),
                ViewMatchers.withParent(linearLayout),
                ViewMatchers.isDisplayed()
            )
        )
        textView.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        val textView2 = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.label_text_view), ViewMatchers.withText("Repositories"),
                ViewMatchers.withParent(linearLayout),
                ViewMatchers.isDisplayed()
            )
        )
        textView2.check(ViewAssertions.matches(ViewMatchers.withText("Repositories")))
    }
}