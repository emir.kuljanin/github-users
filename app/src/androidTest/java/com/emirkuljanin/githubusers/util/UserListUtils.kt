package com.emirkuljanin.githubusers.util

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.scrollTo
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import com.emirkuljanin.githubusers.R
import com.emirkuljanin.githubusers.ui.list.UserListRecyclerAdapter
import com.emirkuljanin.githubusers.util.helper.RecyclerViewChildrenAssertion
import org.hamcrest.Matchers

object UserListUtils {

    fun checkRecyclerViewExists() {
        val recyclerView = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.user_recycler_view),
                ViewMatchers.isDisplayed()
            )
        )
        recyclerView.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    fun checkRecyclerViewItemsExist() {
        val cardView = Espresso.onView(
            Matchers.allOf(
                AutomationUtils.nthChildOf(
                    Matchers.allOf(
                        ViewMatchers.withId(R.id.user_recycler_view),
                    ),
                    1
                ),
                ViewMatchers.isDisplayed()
            )
        )
        cardView.check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    fun clickOnRecyclerViewItem() {
        val recyclerView = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.user_recycler_view)
            )
        )
        recyclerView.perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                1,
                ViewActions.click()
            )
        )
    }

    fun countRecyclerViewChildren(childrenNumber: Int) {
        val recyclerView = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.user_recycler_view)
            )
        )

        recyclerView.check(RecyclerViewChildrenAssertion(childrenNumber))
    }

    //Don't forget to turn off animations on test device or this will break
    fun scrollToItemAtPosition(position: Int) {
        val recyclerView = Espresso.onView(
            Matchers.allOf(
                ViewMatchers.withId(R.id.user_recycler_view)
            )
        )

        recyclerView.perform(
            RecyclerViewActions.actionOnItemAtPosition<UserListRecyclerAdapter.ViewHolder>(
                position,
                scrollTo()
            )
        )
    }
}