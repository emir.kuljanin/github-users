package com.emirkuljanin.githubusers.utils

//API
const val BASE_URL = "https://api.github.com"
const val SINCE_QUERY = "since"
const val LOGIN_ID_PATH = "loginId"

//Sign in
const val SIGN_IN_REQUEST_CODE = 100

//Preferences
const val PREFERENCES_TAG = "git_user_preferences"
const val LOGIN_TOKEN = "login_token"

//User list
const val STARTING_USER_ID = 0L
const val LOGIN_ID = "loginId"