package com.emirkuljanin.githubusers.utils.crypto

import kotlin.math.pow

object DecodeUtils {
    fun decode(s: String): String {
        if (s.isEmpty()) {
            return ""
        }

        //Create the same initial
        //String as in encode class
        val ini = "11111111"
        var flag = true

        //Run a loop of size 8
        for (i in 0..7) {
            //Check if the initial value is same
            if (ini[i] != s[i]) {
                flag = false
                break
            }
        }
        var `val` = ""

        //Reverse the encrypted code
        for (i in 8 until s.length) {
            val ch = s[i]
            `val` += ch.toString()
        }

        //Create a 2 dimensional array
        val arr = Array(11101) { IntArray(8) }
        var ind1 = -1
        var ind2 = 0

        //Run a loop of size of the encrypted code
        for (i in `val`.indices) {

            //Check if the position of the
            //String if divisible by 7
            if (i % 7 == 0) {
                //Start the value in other
                //Column of the 2D array
                ind1++
                ind2 = 0
                val ch = `val`[i]
                arr[ind1][ind2] = ch - '0'
                ind2++
            } else {
                //Otherwise store the value
                //In the same column
                val ch = `val`[i]
                arr[ind1][ind2] = ch - '0'
                ind2++
            }
        }
        //Create an array
        val num = IntArray(11111)
        var nind = 0
        var tem: Int

        //Run a loop of size of the column
        for (i in 0..ind1) {
            tem = 0
            //Convert binary to decimal and add them
            //From each column and store in the array
            for ((cu, j) in (6 downTo 0).withIndex()) {
                val tem1 = 2.0.pow(cu.toDouble()).toInt()
                tem += arr[i][j] * tem1
            }
            num[nind++] = tem
        }
        var ret = ""
        var ch: Char
        // convert the decimal ascii number to its
        // char value and add them to form a decrypted
        // string using conception function
        for (i in 0 until nind) {
            ch = num[i].toChar()
            ret += ch.toString()
        }

        //Check if the encrypted code was
        //Generated for this algorithm
        return if (`val`.length % 7 == 0 && flag) {
            //Return the decrypted code
            ret
        } else {
            //Otherwise return an invalid message
            ""
        }
    }
}