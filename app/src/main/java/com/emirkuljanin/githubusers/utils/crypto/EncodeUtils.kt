package com.emirkuljanin.githubusers.utils.crypto

object EncodeUtils {
    fun encode(s: String): String {
        if (s.isEmpty()) {
            return ""
        }
        //Create a string to add in the initial
        //Binary code for extra security
        val ini = "11111111"
        var cu = 0

        //Create an array
        val arr = IntArray(11111111)

        //Iterate through the string
        for (i in s.indices) {
            //Put the ascii value of
            //Each character in the array
            arr[i] = s[i].toInt()
            cu++
        }
        var res = ""

        //Create another array
        val bin = IntArray(111)
        var idx: Int

        //Run a loop of the size of string
        for (i1 in 0 until cu) {

            //Get the ascii value at position
            //i1 from the first array
            var temp = arr[i1]

            //Run the second nested loop of same size
            //And set 0 value in the second array
            for (j in 0 until cu) bin[j] = 0
            idx = 0

            // run a while for temp > 0
            while (temp > 0) {
                //Store the temp module
                //Of 2 in the 2nd array
                bin[idx++] = temp % 2
                temp /= 2
            }
            var dig = ""
            var temps: String

            //Run a loop of size 7
            for (j in 0..6) {

                //Convert the integer to string
                temps = bin[j].toString()

                //Add the string using
                //Concatenation function
                dig += temps
            }
            var revs = ""

            //Reverse the string
            for (j in dig.length - 1 downTo 0) {
                val ca = dig[j]
                revs += ca.toString()
            }
            res += revs
        }
        //Add the extra string to the binary code
        res = ini + res

        //Return the encrypted code
        return res
    }
}