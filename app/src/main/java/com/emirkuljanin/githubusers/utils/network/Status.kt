package com.emirkuljanin.githubusers.utils.network

//API call status
enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}