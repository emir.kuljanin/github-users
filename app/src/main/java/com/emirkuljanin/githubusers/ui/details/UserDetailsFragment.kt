package com.emirkuljanin.githubusers.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.emirkuljanin.githubusers.R
import com.emirkuljanin.githubusers.databinding.FragmentUserDetailsBinding
import com.emirkuljanin.githubusers.databinding.ItemNumberContainerBinding
import com.emirkuljanin.githubusers.utils.LOGIN_ID
import com.emirkuljanin.githubusers.utils.network.Status
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class UserDetailsFragment : DaggerFragment() {
    @Inject
    lateinit var userDetailsViewModelFactory: UserDetailsViewModelFactory

    private lateinit var binding: FragmentUserDetailsBinding
    private lateinit var viewModel: UserDetailsViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_user_details, container,
            false
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProviders.of(this, userDetailsViewModelFactory)
            .get(UserDetailsViewModel::class.java)

        loadUserDetails()

        super.onViewCreated(view, savedInstanceState)
    }

    private fun loadUserDetails() {
        //Read loginId from arguments
        val loginId = arguments?.getString(LOGIN_ID)
        if (loginId == null) {
            Toast.makeText(requireContext(), getString(R.string.user_load_error), Toast.LENGTH_LONG)
                .show()
            return
        }

        //Get user data
        viewModel.getUserDetails(loginId).observe(viewLifecycleOwner) { response ->
            //Handle response
            when (response.status) {
                Status.SUCCESS -> {
                    //Call successful, handle data and hide progress bar
                    response.data?.let { userDetails ->
                        binding.userDetails = userDetails

                        //Create map of number values to show, and add a view for each of them
                        val numbersMap = mutableMapOf<String, Int>()
                        numbersMap[getString(R.string.user_followers)] = userDetails.followers
                        numbersMap[getString(R.string.user_following)] = userDetails.following
                        numbersMap[getString(R.string.user_repositories)] = userDetails.public_repos

                        inflateNumberContainer(numbersMap)

                        //Load profile image
                        Glide.with(requireContext())
                            .load(userDetails.avatarURL)
                            .circleCrop()
                            .placeholder(R.drawable.ic_profile_placeholder)
                            .into(binding.profilePictureImageView)
                    }

                    binding.isLoading = false
                }

                Status.ERROR -> {
                    //Call failed show error message
                    Toast.makeText(activity, response.message, Toast.LENGTH_SHORT).show()
                    binding.isLoading = false
                }

                Status.LOADING -> {
                    //Show progress bar
                    binding.isLoading = true
                }
            }
        }
    }

    //Receive map of values and add view to container for each of them
    private fun inflateNumberContainer(numberMap: MutableMap<String, Int>) {
        numberMap.forEach {
            val itemBinding = ItemNumberContainerBinding.inflate(
                LayoutInflater.from(context), binding.numbersInfoContainer,
                true
            )

            itemBinding.title = it.value.toString()
            itemBinding.subTitle = it.key
        }
    }
}