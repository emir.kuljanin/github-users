package com.emirkuljanin.githubusers.ui.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

class SplashScreenViewModelFactory @Inject constructor(
    private val splashScreenViewModel: SplashScreenViewModel
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(SplashScreenViewModel::class.java)) {
            return splashScreenViewModel as T
        }

        throw IllegalArgumentException("Unknown class name")
    }
}