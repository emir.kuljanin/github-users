package com.emirkuljanin.githubusers.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.emirkuljanin.githubusers.R
import com.emirkuljanin.githubusers.databinding.FragmentUserListBinding
import com.emirkuljanin.githubusers.ui.list.UserListRecyclerAdapter.UserClickListener
import com.emirkuljanin.githubusers.utils.InfiniteScrollListener
import com.emirkuljanin.githubusers.utils.LOGIN_ID
import com.emirkuljanin.githubusers.utils.STARTING_USER_ID
import com.emirkuljanin.githubusers.utils.network.Status
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class UserListFragment : DaggerFragment(), UserClickListener {
    @Inject
    lateinit var userListViewModelFactory: UserListViewModelFactory

    private lateinit var binding: FragmentUserListBinding
    private lateinit var viewModel: UserListViewModel
    private lateinit var adapter: UserListRecyclerAdapter

    private var lastUserId = STARTING_USER_ID
    private var initialLoad = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_user_list, container,
            false
        )

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel = ViewModelProviders.of(this, userListViewModelFactory)
            .get(UserListViewModel::class.java)

        setUI()

        //TODO: savedInstanceState instead of global
        //Only load on first opening
        if (initialLoad) {
            //Show progress bar and load first list of users
            binding.isLoading = true
            loadMore()

            initialLoad = false
        }

        super.onViewCreated(view, savedInstanceState)
    }

    private fun setUI() {
        //Initialize adapter if not already initialized
        if (!::adapter.isInitialized) {
            adapter = UserListRecyclerAdapter(requireContext(), arrayListOf(), this)
        }

        binding.userRecyclerView.adapter = adapter

        //Set infinite scroll
        val infiniteScrollListener = InfiniteScrollListener {
            loadMore()
        }

        binding.userRecyclerView.addOnScrollListener(infiniteScrollListener)
    }

    //Set observers for user list
    private fun loadMore() {
        viewModel.getUsersList(lastUserId).observe(viewLifecycleOwner) { response ->
            //Handle response
            when (response.status) {
                Status.SUCCESS -> {
                    //Call successful, handle data and hide progress bar
                    response.data?.let { userList ->
                        adapter.apply {
                            addUsers(userList)
                            //For initial state prevent negative index
                            var startPosition = adapter.getUsers().count() - 1
                            if (startPosition < 0) {
                                startPosition = 0
                            }

                            lastUserId = adapter.getLastUserId()
                            notifyItemRangeInserted(startPosition, userList.count())
                            binding.isLoading = false
                        }
                    }
                }

                Status.ERROR -> {
                    //Call failed show error message
                    Toast.makeText(activity, response.message, Toast.LENGTH_SHORT).show()
                    binding.isLoading = false
                }

                Status.LOADING -> {
                    //Show progress bar
                    binding.isLoading = true
                }
            }
        }
    }

    //Navigate to user details for selected user
    override fun onUserSelected(loginId: String) {
        findNavController().navigate(
            R.id.action_userListFragment_to_userDetailsFragment,
            bundleOf(LOGIN_ID to loginId)
        )
    }
}