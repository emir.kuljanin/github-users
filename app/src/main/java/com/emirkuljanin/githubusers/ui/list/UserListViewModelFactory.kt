package com.emirkuljanin.githubusers.ui.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

class UserListViewModelFactory @Inject constructor(
    private val userListViewModel:
    UserListViewModel
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserListViewModel::class.java)) {
            return userListViewModel as T
        }

        throw IllegalArgumentException("Unknown class name")
    }
}