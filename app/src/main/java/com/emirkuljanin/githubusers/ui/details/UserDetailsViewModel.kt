package com.emirkuljanin.githubusers.ui.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.emirkuljanin.githubusers.data.repository.UserRepository
import com.emirkuljanin.githubusers.utils.network.Resource
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class UserDetailsViewModel @Inject constructor(private val repository: UserRepository) :
    ViewModel() {

    //Get user list starting from id after passed one
    fun getUserDetails(loginId: String) = liveData(Dispatchers.IO) {
        //Start loading
        emit(Resource.loading(data = null))
        try {
            //Success, send data
            emit(Resource.success(data = repository.getUserDetails(loginId)))
        } catch (exception: Exception) {
            //Fail, show error message, or return if API returned null
            emit(
                Resource.error(
                    data = null,
                    message = exception.message ?: return@liveData
                )
            )
        }
    }
}