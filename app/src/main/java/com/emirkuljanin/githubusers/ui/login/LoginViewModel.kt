package com.emirkuljanin.githubusers.ui.login

import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import com.emirkuljanin.githubusers.utils.LOGIN_TOKEN
import com.emirkuljanin.githubusers.utils.crypto.EncodeUtils
import javax.inject.Inject

class LoginViewModel @Inject constructor(private val sharedPreferences: SharedPreferences) :
    ViewModel() {

    //Save login to preferences, no need to hold user while it happens
    fun saveLoginData(loginToken: String) {
        //Encrypt token for security

        //TODO: jetpack security lib
        val encryptedLoginToken = EncodeUtils.encode(loginToken)
        sharedPreferences.edit().putString(LOGIN_TOKEN, encryptedLoginToken).apply()
    }
}