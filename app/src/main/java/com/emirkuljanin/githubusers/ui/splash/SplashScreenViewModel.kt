package com.emirkuljanin.githubusers.ui.splash

import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.emirkuljanin.githubusers.utils.LOGIN_TOKEN
import com.emirkuljanin.githubusers.utils.crypto.DecodeUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import javax.inject.Inject

class SplashScreenViewModel @Inject constructor(private val sharedPreferences: SharedPreferences) :
    ViewModel() {

    //Check if user previously logged in and notify fragment
    fun isLoggedIn() = liveData(Dispatchers.IO) {
        //Delay for splash screen to show
        delay(2_000)

        //Get encrypted token from preferences
        val encryptedToken = sharedPreferences.getString(LOGIN_TOKEN, "")

        //Decrypt token and do something with it?
        val token = DecodeUtils.decode(encryptedToken ?: "")

        emit(token.isNotEmpty())
    }
}