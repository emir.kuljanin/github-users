package com.emirkuljanin.githubusers.ui.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import com.emirkuljanin.githubusers.data.repository.UserListRepository
import com.emirkuljanin.githubusers.utils.network.Resource
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class UserListViewModel @Inject constructor(private val repository: UserListRepository) :
    ViewModel() {
    
    //Get user list starting from id after passed one
    fun getUsersList(userId: Long) = liveData(Dispatchers.IO) {
        //Start loading
        emit(Resource.loading(data = null))
        try {
            //Success, send data
            emit(Resource.success(data = repository.getUsersList(userId)))
        } catch (exception: Exception) {
            //Fail, show error message, or return if API returned null
            emit(
                Resource.error(
                    data = null,
                    message = exception.message ?: return@liveData
                )
            )
        }
    }
}