package com.emirkuljanin.githubusers.ui.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject

class UserDetailsViewModelFactory @Inject constructor(
    private val userDetailsViewModel:
    UserDetailsViewModel
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(UserDetailsViewModel::class.java)) {
            return userDetailsViewModel as T
        }

        throw IllegalArgumentException("Unknown class name")
    }
}