package com.emirkuljanin.githubusers.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.emirkuljanin.githubusers.R
import com.emirkuljanin.githubusers.databinding.ActivityMainBinding
import com.emirkuljanin.githubusers.ui.login.LoginFragment
import dagger.android.support.DaggerAppCompatActivity

class MainActivity : DaggerAppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Set binding
        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)

        //Configure toolbar with navigation component
        setNavigation()
    }

    //Trigger onActivityResult for LoginFragment
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            if (fragment is LoginFragment) {
                fragment.onActivityResult(requestCode, resultCode, data)
                break
            }
        }
    }

    private fun setNavigation() {
        //Get navigation controller
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        val navController = navHostFragment.navController

        //Hide toolbar for splash screen and login
        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.splashScreenFragment, R.id.loginFragment -> {
                    binding.toolbar.visibility =
                        View.GONE
                }
                else -> {
                    binding.toolbar.visibility = View.VISIBLE
                }
            }
        }

        //Set fragments as top level
        val appBarConfiguration =
            AppBarConfiguration.Builder(
                R.id.splashScreenFragment,
                R.id.loginFragment,
                R.id.userListFragment
            ).build()

        //Setup toolbar with navigation controller
        binding.toolbar.setupWithNavController(navController, appBarConfiguration)
    }
}