package com.emirkuljanin.githubusers.ui.list

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.emirkuljanin.githubusers.R
import com.emirkuljanin.githubusers.data.model.User
import com.emirkuljanin.githubusers.databinding.ItemUserListBinding

class UserListRecyclerAdapter(
    private val context: Context,
    private val userList: ArrayList<User>,
    private val userClickListener: UserClickListener
) : RecyclerView.Adapter<UserListRecyclerAdapter.ViewHolder>() {
    interface UserClickListener {
        fun onUserSelected(loginId: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemBinding = ItemUserListBinding.inflate(
            LayoutInflater.from(context), parent,
            false
        )

        return ViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = userList[position]

        //Use login handle as name
        holder.binding.name = user.login

        //Load image into view
        Glide.with(context)
            .load(user.avatar_url)
            .circleCrop()
            .placeholder(R.drawable.ic_profile_placeholder)
            .into(holder.binding.profilePictureImageView)
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    fun addUsers(userList: List<User>) {
        this.userList.addAll(userList)
    }

    fun getUsers(): List<User> {
        return userList
    }

    fun getLastUserId(): Long {
        return userList[itemCount - 1].id
    }

    inner class ViewHolder(val binding: ItemUserListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        //Add on click listener to item, and notify fragment
        init {
            binding.root.setOnClickListener {
                userClickListener.onUserSelected(userList[bindingAdapterPosition].login)
            }
        }
    }
}