package com.emirkuljanin.githubusers.ui.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.emirkuljanin.githubusers.R
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class SplashScreenFragment : DaggerFragment() {
    @Inject
    lateinit var splashScreenViewModelFactory: SplashScreenViewModelFactory

    private lateinit var viewModel: SplashScreenViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_splash_screen, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this, splashScreenViewModelFactory)
            .get(SplashScreenViewModel::class.java)

        //Check user login status and observe for result
        viewModel.isLoggedIn().observe(viewLifecycleOwner, { loggedIn ->
            //If logged in open user list, else open login screen
            if (loggedIn) {
                findNavController().navigate(
                    R.id.action_splashScreenFragment_to_userListFragment
                )
            } else {
                findNavController().navigate(
                    R.id.action_splashScreenFragment_to_loginFragment
                )
            }
        })
    }
}