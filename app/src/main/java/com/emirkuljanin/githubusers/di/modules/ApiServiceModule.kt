package com.emirkuljanin.githubusers.di.modules

import com.emirkuljanin.githubusers.data.api.UsersService
import dagger.Module
import dagger.Provides
import dagger.Reusable
import retrofit2.Retrofit

@Module
object ApiServiceModule {

    @Provides
    @Reusable
    internal fun provideUsersService(retrofit: Retrofit): UsersService {
        return retrofit.create(UsersService::class.java)
    }
}