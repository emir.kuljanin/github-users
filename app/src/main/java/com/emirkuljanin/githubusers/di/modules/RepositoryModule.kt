package com.emirkuljanin.githubusers.di.modules

import com.emirkuljanin.githubusers.data.api.UsersService
import com.emirkuljanin.githubusers.data.repository.UserListRepository
import com.emirkuljanin.githubusers.data.repository.UserRepository
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
object RepositoryModule {

    @Provides
    @Reusable
    internal fun provideUsersListRepository(usersService: UsersService):
            UserListRepository {
        return UserListRepository(usersService)
    }

    @Provides
    @Reusable
    internal fun provideUsersRepository(usersService: UsersService):
            UserRepository {
        return UserRepository(usersService)
    }
}