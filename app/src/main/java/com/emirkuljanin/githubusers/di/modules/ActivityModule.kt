package com.emirkuljanin.githubusers.di.modules

import com.emirkuljanin.githubusers.ui.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract fun provideMainActivity(): MainActivity
}