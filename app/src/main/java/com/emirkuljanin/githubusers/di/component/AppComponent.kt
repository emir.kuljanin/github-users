package com.emirkuljanin.githubusers.di.component

import com.emirkuljanin.githubusers.Application
import com.emirkuljanin.githubusers.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidSupportInjectionModule::class,
        NetworkModule::class,
        ViewModelModule::class,
        ApiServiceModule::class,
        RepositoryModule::class,
        PreferencesModule::class,
        ActivityModule::class,
        FragmentModule::class]
)

interface AppComponent : AndroidInjector<Application> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}