package com.emirkuljanin.githubusers.di.modules

import androidx.lifecycle.ViewModelProvider
import com.emirkuljanin.githubusers.ui.details.UserDetailsViewModelFactory
import com.emirkuljanin.githubusers.ui.list.UserListViewModelFactory
import com.emirkuljanin.githubusers.ui.login.LoginViewModelFactory
import com.emirkuljanin.githubusers.ui.splash.SplashScreenViewModelFactory
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
object ViewModelModule {

    @Provides
    @Reusable
    internal fun provideUserDetailsViewModelFactory(
        userDetailsViewModelFactory:
        UserDetailsViewModelFactory
    ): ViewModelProvider.Factory =
        userDetailsViewModelFactory

    @Provides
    @Reusable
    internal fun provideUserListViewModelFactory(
        userListViewModelFactory:
        UserListViewModelFactory
    ):
            ViewModelProvider.Factory = userListViewModelFactory

    @Provides
    @Reusable
    internal fun provideLoginViewModelFactory(loginViewModelFactory: LoginViewModelFactory):
            ViewModelProvider.Factory = loginViewModelFactory

    @Provides
    @Reusable
    internal fun provideSplashScreenViewModelFactory(
        splashScreenViewModelFactory:
        SplashScreenViewModelFactory
    ): ViewModelProvider.Factory = splashScreenViewModelFactory
}