package com.emirkuljanin.githubusers.di.modules

import android.content.Context
import android.content.SharedPreferences
import com.emirkuljanin.githubusers.Application
import com.emirkuljanin.githubusers.utils.PREFERENCES_TAG
import dagger.Module
import dagger.Provides


@Module
object PreferencesModule {

    @Provides
    internal fun provideSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(PREFERENCES_TAG, Context.MODE_PRIVATE)
    }

    @Provides
    internal fun provideContext(app: Application): Context = app
}