package com.emirkuljanin.githubusers.di.modules

import com.emirkuljanin.githubusers.ui.details.UserDetailsFragment
import com.emirkuljanin.githubusers.ui.list.UserListFragment
import com.emirkuljanin.githubusers.ui.login.LoginFragment
import com.emirkuljanin.githubusers.ui.splash.SplashScreenFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun provideSplashScreenFragment(): SplashScreenFragment

    @ContributesAndroidInjector
    abstract fun provideLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun provideUserListFragment(): UserListFragment

    @ContributesAndroidInjector
    abstract fun provideUserDetailsFragment(): UserDetailsFragment
}
