package com.emirkuljanin.githubusers.data.api

import com.emirkuljanin.githubusers.data.model.User
import com.emirkuljanin.githubusers.data.model.UserDetails
import com.emirkuljanin.githubusers.utils.LOGIN_ID_PATH
import com.emirkuljanin.githubusers.utils.SINCE_QUERY
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface UsersService {

    @GET("/users")
    suspend fun getUsers(@Query(SINCE_QUERY) userId: Long): Call<List<User>>

    @GET("/users/{$LOGIN_ID_PATH}")
    suspend fun getUserDetails(@Path(LOGIN_ID_PATH) loginId: String) : UserDetails
}