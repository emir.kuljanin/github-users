package com.emirkuljanin.githubusers.data.repository

import com.emirkuljanin.githubusers.data.api.UsersService
import com.emirkuljanin.githubusers.data.model.User
import javax.inject.Inject

class UserListRepository @Inject constructor(
    private val usersService: UsersService) {

    private var userList: List<User> = arrayListOf()

    //TODO: handle here not in vm
    //Get user list using the userId after the passed one for the first item
    suspend fun getUsersList(userId: Long) : List<User> {
        if (userList.isEmpty()) {
            val response = usersService.getUsers(userId).execute()

            response.isSuccessful
            userList =
        }

        return userList
    }
}