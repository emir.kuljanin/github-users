package com.emirkuljanin.githubusers.data.repository

import com.emirkuljanin.githubusers.data.api.UsersService
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val usersService: UsersService
) {
    //Get details for passed loginId
    suspend fun getUserDetails(loginId: String) = usersService.getUserDetails(loginId)
}