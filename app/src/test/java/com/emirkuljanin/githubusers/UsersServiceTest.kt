package com.emirkuljanin.githubusers

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.emirkuljanin.githubusers.data.api.UsersService
import com.emirkuljanin.githubusers.data.model.User
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@RunWith(JUnit4::class)
class UsersServiceTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var usersService: UsersService
    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService() {
        mockWebServer = MockWebServer()
        usersService = Retrofit.Builder()
            .baseUrl(mockWebServer.url(""))
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create(UsersService::class.java)
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun getUsers() {
        runBlocking {
            enqueueResponse("users.json")
            val resultResponse = usersService.getUsers(0)

            val request = mockWebServer.takeRequest()
            assertNotNull(resultResponse)
            assertThat(request.path, CoreMatchers.`is`("/users?since=0"))

            resultResponse.forEach {
                verifyUserData(it)
            }
        }
    }

    @Test
    fun getUserDetails() {
        runBlocking {
            enqueueResponse("userdetails.json")
            val resultResponse = usersService.getUserDetails("lukas")

            val request = mockWebServer.takeRequest()
            assertNotNull(resultResponse)
            assertThat(request.path, CoreMatchers.`is`("/users/lukas"))

            assertNotNull(resultResponse.avatar_url)
            assertNotNull(resultResponse.bio)
            assertNotNull(resultResponse.name)
            assertNotNull(resultResponse.followers)
            assertNotNull(resultResponse.following)
            assertNotNull(resultResponse.public_repos)
        }
    }

    //Verify necessary data
    private fun verifyUserData(user: User) {
        assertNotNull(user.id)
        assertNotNull(user.login)
        assertNotNull(user.avatar_url)
    }

    private fun enqueueResponse(fileName: String, headers: Map<String, String> = emptyMap()) {
        val inputStream = javaClass.classLoader
            ?.getResourceAsStream("apiresponse/$fileName")
        val source = inputStream?.source()?.buffer()
        val mockResponse = MockResponse()
        for ((key, value) in headers) {
            mockResponse.addHeader(key, value)
        }

        if (source != null) {
            mockWebServer.enqueue(
                mockResponse.setBody(
                    source.readString(Charsets.UTF_8)
                )
            )
        }
    }
}