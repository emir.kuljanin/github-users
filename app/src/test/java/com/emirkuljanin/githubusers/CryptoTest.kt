package com.emirkuljanin.githubusers

import com.emirkuljanin.githubusers.utils.crypto.DecodeUtils
import com.emirkuljanin.githubusers.utils.crypto.EncodeUtils
import org.hamcrest.CoreMatchers
import org.junit.Assert
import org.junit.Test

class CryptoTest {
    private val testString = "TestingThis"
    private val encryptedTestString = "1111111110101001100101111001111101001101001110111011001111010100110100011010011110011"

    @Test
    fun testEncryptionBadData() {
        val encryptedString = EncodeUtils.encode("")

        Assert.assertThat(encryptedString, CoreMatchers.`is`(""))
    }

    @Test
    fun testEncryptionGoodData() {
        val encryptedString = EncodeUtils.encode(testString)

        Assert.assertThat(encryptedString, CoreMatchers.`is`(encryptedTestString))
    }

    @Test
    fun testDecryptionBadData() {
        val string = DecodeUtils.decode("")

        Assert.assertThat(string, CoreMatchers.`is`(""))
    }

    @Test
    fun testDecryptionGoodData() {
        val string = DecodeUtils.decode(encryptedTestString)

        Assert.assertThat(string, CoreMatchers.`is`(testString))
    }
}